package com.trio.ronelio.takehome.contact;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static com.fasterxml.jackson.databind.type.LogicalType.Map;

@Service
public class ContactService {

  private final RestTemplate restHandler;

  @Value("${api.endpoint.mailchimp}")
  private String mailChimpEndpoint;

  @Value("${auth.token.mailchimp}")
  private String mailChimpToken;

  @Value("${api.endpoint.sendgrid}")
  private String sendGridEndpoint;

  @Value("${auth.token.sendgrid}")
  private String sendGridToken;

  ContactService(RestTemplateBuilder restTemplateBuilder){
    this.restHandler = restTemplateBuilder.build();
  }

  public boolean syncWithSendGrid(ArrayList<Contact> contacts){
    var headers = new HttpHeaders();
    headers.setBearerAuth(this.sendGridToken);
    headers.setContentType(MediaType.APPLICATION_JSON);

    var contactToPut = new HashMap();
    contactToPut.put("contacts", contacts);
    HttpEntity<HashMap> entity = new HttpEntity<>(contactToPut, headers);

    try{
      var response = this.restHandler.exchange(
              this.sendGridEndpoint,
              HttpMethod.PUT,
              entity,
              Object.class);

      if (response.getStatusCode() == HttpStatus.ACCEPTED) {
        return true;
      } else {
        System.err.println("SENDGRID: Error on PUT contacts list ==> " + response.getStatusCode().toString());
      }
    }catch (RestClientException e){
      System.err.println("SENDGRID: Error on PUT contacts list ==> " + e.getMessage());
    }

    return false;
  }

  public ArrayList<Contact> getContactsFromMailChimp() {
    var headers = new HttpHeaders();
    headers.add("Authorization", "Basic " + this.mailChimpToken);

    var entity = new HttpEntity(headers);
    var contactList = new ArrayList<Contact>();

    try {
      var response = this.restHandler.exchange(
              this.mailChimpEndpoint,
              HttpMethod.GET,
              entity,
              LinkedHashMap.class
      );

      var bodyEntity = response.getBody();
      var memberList = (ArrayList) bodyEntity.get("lists");

      if (memberList.size() < 0) {
        return null;
      }
      var listInfo = (LinkedHashMap) memberList.get(0);
      var listId = listInfo.get("id").toString();

      if (listId.isEmpty()) {
        return null;
      }

      var resposeMembers = this.restHandler.exchange(
              this.mailChimpEndpoint + listId + "/members",
              HttpMethod.GET,
              entity,
              LinkedHashMap.class
      );

      try {

        var members = (ArrayList) (resposeMembers.getBody()).get("members");
        for (Object mObj : members) {
          LinkedHashMap member = (LinkedHashMap) mObj;
          LinkedHashMap merge_fields = (LinkedHashMap) member.get("merge_fields");

          var contact = new Contact();
          contact.setFirstName(merge_fields.get("FNAME").toString());
          contact.setLastName(merge_fields.get("LNAME").toString());
          contact.setEmail(member.get("email_address").toString());

          contactList.add(contact);
        }
      } catch (Exception e) {
        System.err.println("Error on parsing contact list ==> " + e.getMessage());
      }

    } catch (RestClientException e) {
      System.err.println("MAILCHIMP: Error on request contact list ==> " + e.getMessage());
    }
    return contactList;
  }

}
