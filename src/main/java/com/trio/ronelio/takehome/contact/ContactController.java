package com.trio.ronelio.takehome.contact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;

@RestController
public class ContactController {

  @Autowired
  ContactService contactService;

  @GetMapping(path = "/contacts/sync")
  public ResponseEntity<?> syncContacts(){

    var arrContacts = contactService.getContactsFromMailChimp();
    var response = new LinkedHashMap();
    response.put("syncedContacts", arrContacts.size());
    response.put("contacts", arrContacts);

    if(contactService.syncWithSendGrid(arrContacts)) {
      return ResponseEntity.ok(response);
    }else{
      return ResponseEntity.internalServerError().build();
    }
  }
}
