
# TRIO take-home code

Minimal [Spring Boot](http://projects.spring.io/spring-boot/).

This project is a tool that syncs contacts from **Mailchimp** to **Sendgrid** using *Spring REST API*.

## Requirements

For building and running the application you need:

- [JDK 11](https://www.oracle.com/br/java/technologies/javase-jdk11-downloads.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.trio.ronelio.takehome.ContactsApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

For debugging:
```shell
spring-boot:run -Dspring-boot.run.fork=false
```
## Before running

You need to configure the application.properties as follows:

    api.endpoint.mailchimp= 
    api.endpoint.sendgrid=  
    auth.token.mailchimp=  
    auth.token.sendgrid=

Provide parameters according to endpoint information.

## Endpoints

  

### Open Endpoints

Endpoints that require no Authentication.

*  [Sync](sync.md) : `GET /contacts/sync`

## Deploying the application to Heroku

The easiest way to deploy this project to Heroku is to use the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

## Copyright

Released under the Apache License 2.0. See the [LICENSE](https://github.com/codecentric/springboot-sample-app/blob/master/LICENSE) file.
