
# Sync
Used to sync contacts between *MailChimp* and *SendGrid* services

****URL**** : `/contacts/sync`
****Method**** : `GET`
****Auth required**** : NO

****Data constraints****

```
NO INPUT REQUIRED
```

## Success Response

****Code**** : `200 OK`
****Content example****

``` json
{
  "syncedContacts": 1,
  "contacts": [
    {
      "firstName": "Amelia",
      "lastName": "Earhart",
      "email": "amelia_earhart@gmail.com"
    }
  ]
}
```

## Error Response
****Condition**** : If *application.properties* is missing parameters.
****Code**** : `500 Internal server error`
****Content**** :

```
An internal server error has occurred.
```
